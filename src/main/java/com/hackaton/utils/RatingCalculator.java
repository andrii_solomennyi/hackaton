package com.hackaton.utils;

import com.hackaton.dto.Event;
import com.hackaton.dto.MpAthlete;

import java.util.List;

public class RatingCalculator {
    public static int calculateRating(List<Event> allEvents, MpAthlete athlete) {
        return allEvents.stream()
                .filter(event -> isAthleteParticipated(event, athlete))
                .map(event -> event.getComplexity().getXp())
                .reduce(0, Integer::sum);

    }

    public static boolean isAthleteParticipated(Event event, MpAthlete athlete) {
        return event.getParticipants().stream()
                .anyMatch(a -> a.getId() == athlete.getId());
    }
}
