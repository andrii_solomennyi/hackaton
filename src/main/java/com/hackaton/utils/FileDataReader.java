package com.hackaton.utils;

import com.hackaton.dto.Complexity;
import com.hackaton.dto.Event;

import java.io.*;
import java.util.*;

public class FileDataReader {
    public static List<Event> readEvents() throws Exception {
        BufferedReader in = new BufferedReader(
                new FileReader("src/main/resources/events.txt"));
        List<Event> events = new ArrayList<>();
        String textRef;
        while ((textRef = in.readLine()) != null) {
            String[] fields = textRef.split(",");
            Event event = new Event();
            event.setId(Integer.parseInt(fields[0]));
            event.setName(fields[1]);
            event.setDescription(fields[2]);
            event.setType(fields[3]);
            event.setFromTimestamp(Long.parseLong(fields[4]));
            event.setToTimestamp(Long.parseLong(fields[5]));
            event.setLocation(fields[6]);
            event.setDistance(Double.parseDouble(fields[7]));
            event.setComplexity(Complexity.valueOf(Complexity.class, fields[8].toUpperCase()));
            events.add(event);
        }
        return events;
    }
}
