package com.hackaton.auth;

import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Component
public class MpHttpClient {

    static final int CLIENT_ID = 104382;


    private static final Logger LOGGER = LoggerFactory.getLogger(MpHttpClient.class);

    CloseableHttpClient client = HttpClients.createDefault();

    String get(String uri, String token) {
        HttpUriRequest get = RequestBuilder.get()
            .setUri(uri)
            .setHeader(createContentType())
            .setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token)
            .build();

        ResponseData response = execute(get);

        if (!response.isSuccessful()) {
            LOGGER.warn("Response returned '{}' code", response.getStatus());
        }

        return response.getBody();
    }

    String post(String uri, String body) {
        HttpUriRequest post = RequestBuilder.post()
            .setUri(uri)
            .setEntity(new StringEntity(body, StandardCharsets.UTF_8))
            .setHeader(createContentType())
            .build();

        ResponseData response = execute(post);

        if (!response.isSuccessful()) {
            LOGGER.warn("Response has status '{}'", response.getStatus());
        }

        return response.getBody();
    }

    private Header createContentType() {
        return new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
    }

    private ResponseData execute(HttpUriRequest request) {
        try (CloseableHttpResponse response = client.execute(request)) {
            return ResponseData.valueOf(response);
        } catch (IOException e) {
            return ResponseData.serviceUnavailable();
        }
    }
}
