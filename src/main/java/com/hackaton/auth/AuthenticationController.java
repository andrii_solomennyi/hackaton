package com.hackaton.auth;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import static com.hackaton.auth.MpHttpClient.CLIENT_ID;

@RestController
public class AuthenticationController {

    private static final String REDIRECT_URI = "http://localhost:3000";
    private static final String AUTH_URL = "https://www.strava.com/oauth/authorize?client_id=" + CLIENT_ID
            + "&redirect_uri=" + REDIRECT_URI + "&response_type=code&approval_prompt=auto" +
        "&scope=read_all,profile:read_all,activity:read_all";

    private final MpTokenService tokenService;
    private final MpApiService apiService;

    AuthenticationController(MpTokenService tokenService, MpApiService apiService) {
        this.tokenService = tokenService;
        this.apiService = apiService;
    }

    @GetMapping("/login")
    public ModelAndView login() {
        return new ModelAndView("redirect:" + AUTH_URL);
    }

    @GetMapping("/authenticate")
    public ModelAndView authenticate(@RequestParam String code) {
        tokenService.add(apiService.oauthToken(code));

        return new ModelAndView("redirect:localhost:3000");
    }
}
