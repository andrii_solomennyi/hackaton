package com.hackaton.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class MpTokenRepo {

    private static final Logger LOGGER = LoggerFactory.getLogger(MpTokenRepo.class);

    private final Map<String, MpToken> cache = new HashMap<>();

    Optional<MpToken> get(String sessionId) {
        MpToken token = cache.get(sessionId);

        if (token == null) {
            LOGGER.warn("No token found in repo");
            return Optional.empty();
        }

        long expiresAt = token.getExpiresAt() * 1_000L;
        long now = System.currentTimeMillis();
        long tenMinutes = 10 * 60 * 1_000;

        if (expiresAt - now < tenMinutes) {
            LOGGER.warn("Token has expired");
            cache.remove(sessionId);
            return Optional.empty();
        }

        return Optional.of(token);
    }

    void put(String sessionId, MpToken token) {
        cache.put(sessionId, token);
    }
}
