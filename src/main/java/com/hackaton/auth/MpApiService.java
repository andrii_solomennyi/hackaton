package com.hackaton.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.hackaton.dto.Activity;
import com.hackaton.dto.MpAthlete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.hackaton.auth.MpHttpClient.CLIENT_ID;

@Component
public class MpApiService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MpApiService.class);

    private final MpHttpClient httpClient;

    MpApiService(MpHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    private static final String
        ACCESS_TOKEN = "access_token",
        EXPIRES_AT = "expires_at";

    private static final String
        ATHLETE = "athlete",
        ID = "id",
        FIRST_NAME = "firstname",
        LAST_NAME = "lastname",
        CREATED_AT = "created_at";

    private static final String CLIENT_SECRET = "2f3de28ebcd8527e94f2df61989ddd6a8a5e599a";

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public MpToken oauthToken(String code) {
        String uri = "https://www.strava.com/api/v3/oauth/token";

        String body = "{\n" +
            "\"client_id\": " + CLIENT_ID + ",\n" +
            "\"client_secret\": \"" + CLIENT_SECRET + "\",\n" +
            "\"code\": \"" + code + "\",\n" +
            "\"grant_type\": \"authorization_code\"\n" +
            "}";

        String response = httpClient.post(uri, body);

        JsonNode jsonNode = toJsonNode(response);
        String accessToken = jsonNode.get(ACCESS_TOKEN).asText();
        int expiresAt = jsonNode.get(EXPIRES_AT).asInt();

        JsonNode athleteNode = jsonNode.get(ATHLETE);

        int id = athleteNode.get(ID).intValue();
        String firstname = athleteNode.get(FIRST_NAME).textValue();
        String lastName = athleteNode.get(LAST_NAME).textValue();
        String createdAt = athleteNode.get(CREATED_AT).textValue();

        MpAthlete athlete = new MpAthlete(id, firstname, lastName, createdAt);

        return new MpToken(accessToken, expiresAt, athlete);
    }

    private static JsonNode toJsonNode(String response) {
        JsonNode jsonNode;
        try {
            jsonNode = MAPPER.readValue(response, JsonNode.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return jsonNode;
    }

    public MpAthlete athlete(String token) {
        String uri = "https://www.strava.com/api/v3/athlete";

        String response = httpClient.get(uri, token);

        JsonNode jsonNode = toJsonNode(response);

        int id = jsonNode.get(ID).intValue();
        String firstname = jsonNode.get(FIRST_NAME).textValue();
        String lastName = jsonNode.get(LAST_NAME).textValue();
        String createdAt = jsonNode.get(CREATED_AT).textValue();

        return new MpAthlete(id, firstname, lastName, createdAt);
    }

    public List<Activity> athleteActivities (String token, long before, long after) {
        String uriTemplate = "https://www.strava.com/api/v3/athlete/activities?before=%d&after=%d&page=%d&per_page=%d";
        String uri = String.format(uriTemplate, before, after, 1, 99);

        String response = httpClient.get(uri, token);

        List<Activity> activities = new ArrayList<>();
        ArrayNode arrayNode = toArrayNode(response);

        for (Iterator<JsonNode> it = arrayNode.elements(); it.hasNext(); ) {
            JsonNode jsonNode = it.next();

            Activity activity = new Activity();
            activity.setType(jsonNode.get("type").asText());
            activity.setDistance(jsonNode.get("distance").asDouble());
            activity.setMovingTime(jsonNode.get("moving_time").asDouble());

            String startDateString = jsonNode.get("start_date").asText();
            activity.setStartedDateTime(LocalDateTime.parse(startDateString,
                    DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")));

            activities.add(activity);
        }

        return activities;
    }

    private ArrayNode toArrayNode(String response) {
        ArrayNode arrayNode;
        try {
            arrayNode = MAPPER.readValue(response, ArrayNode.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return arrayNode;
    }
}
