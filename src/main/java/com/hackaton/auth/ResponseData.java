package com.hackaton.auth;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.apache.http.HttpStatus.*;

class ResponseData {

    private int status;
    private String body;
    private String reason;

    private ResponseData(int status) {
        this.status = status;
    }

    private ResponseData(HttpResponse response) {
        status = getResponseCode(response);

        HttpEntity entity = response.getEntity();
        if (entity == null) {
            return;
        }

        body = getResponseBody(entity);
        reason = getErrorReason(body);
    }

    private static int getResponseCode(HttpResponse response) {
        return response.getStatusLine().getStatusCode();
    }

    private static String getResponseBody(HttpEntity entity) {
        try {
            return EntityUtils.toString(entity, StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private static String getErrorReason(String body) {
        JsonNode result;
        try {
            result = new ObjectMapper().readValue(body, JsonNode.class);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        if (result == null) {
            return null;
        }

        JsonNode error = result.get("error");
        if (error == null) {
            return null;
        }

        JsonNode reason = error.get("reason");
        if (reason == null) {
            return null;
        }

        return reason.asText();
    }

    static ResponseData valueOf(HttpResponse response) {
        return new ResponseData(response);
    }

    static ResponseData serviceUnavailable() {
        return new ResponseData(SC_SERVICE_UNAVAILABLE);
    }

    int getStatus() {
        return status;
    }

    boolean isSuccessful() {
        return SC_OK == status;
    }

    boolean isCreated() {
        return SC_CREATED == status;
    }

    boolean isNotFound() {
        return SC_NOT_FOUND == status;
    }

    boolean isIndexAlreadyExists() {
        return SC_BAD_REQUEST == status && reason != null && reason.contains("exists");
    }

    boolean isServiceUnavailable() {
        return SC_SERVICE_UNAVAILABLE == status;
    }

    String getReason() {
        return reason;
    }

    String getBody() {
        return body;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
            "status=" + status +
            ", body='" + body + '\'' +
            ", reason='" + reason + '\'' +
            '}';
    }
}