package com.hackaton.auth;

import com.hackaton.dto.MpAthlete;

import java.util.Objects;
import java.util.StringJoiner;

public class MpToken {

    private final String accessToken;
    private final int expiresAt;
    private final MpAthlete athlete;

    public MpToken(String accessToken, int expiresAt, MpAthlete athlete) {
        this.accessToken = accessToken;
        this.expiresAt = expiresAt;
        this.athlete = athlete;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public int getExpiresAt() {
        return expiresAt;
    }

    public MpAthlete getAthlete() {
        return athlete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MpToken mpToken = (MpToken) o;
        return accessToken.equals(mpToken.accessToken);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accessToken);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", MpToken.class.getSimpleName() + "[", "]")
            .add("accessToken='" + accessToken + "'")
            .add("expiresAt=" + expiresAt)
            .add("athlete=" + athlete)
            .toString();
    }
}
