package com.hackaton.auth;

import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Optional;

@Component
public class MpTokenService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MpTokenService.class);

    private final MpTokenRepo tokenRepo;

    public MpTokenService(MpTokenRepo tokenRepo) {
        this.tokenRepo = tokenRepo;
    }

    public Optional<MpToken> get() {
        HttpSession session = session();

        if (session == null) {
            LOGGER.warn("No active session found");
            return Optional.empty();
        }

        Optional<MpToken> tokenOpt = tokenRepo.get(session.getId());

        if (tokenOpt.isEmpty()) {
            LOGGER.info("Invalidating session due to expired token");
            session.invalidate();
        }

        return tokenOpt;
    }

    private static HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession();
    }

    public void add(MpToken token) {
        HttpSession session = sessionWithCreate();

        if (session == null) {
            LOGGER.warn("No active session found");
            return;
        }

        tokenRepo.put(session.getId(), token);
    }

    private static HttpSession sessionWithCreate() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession();
    }
}
