package com.hackaton.repo;

import com.hackaton.dto.Event;
import com.hackaton.utils.FileDataReader;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class EventRepo {
    private final Map<Integer, Event> cache = new HashMap<>();

    public void create(Event event) {
        cache.put(event.getId(), event);
    }

    public Event read(int id) {
        exists(id);
        return cache.get(id);
    }

    public void update(Event event) {
        exists(event.getId());
        create(event);
    }

    public void delete(int id) {
        exists(id);
        cache.remove(id);
    }

    private void exists(int id) {
        if(!cache.containsKey(id)){
            throw new IllegalArgumentException("Event with ID " + id + " not found");
        }
    }

    public List<Event> getAllEvents() {
        return new ArrayList<>(cache.values());
    }

    @PostConstruct
    public void fillRepo() {
        List<Event> basicEvents;
        try {
            basicEvents = FileDataReader.readEvents();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        for(Event event : basicEvents) {
            create(event);
        }
    }
}
