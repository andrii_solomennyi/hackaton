package com.hackaton.service;

import com.hackaton.auth.MpApiService;
import com.hackaton.auth.MpToken;
import com.hackaton.auth.MpTokenService;
import com.hackaton.dto.Event;
import com.hackaton.dto.Stats;
import com.hackaton.dto.Activity;
import com.hackaton.dto.MpAthlete;
import com.hackaton.repo.EventRepo;
import com.hackaton.utils.RatingCalculator;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class EventService {
    private final EventRepo repo;
    private final MpTokenService tokenService;
    private final MpApiService apiService;

    public EventService(EventRepo repo, MpTokenService tokenRepo, MpApiService apiService) {
        this.repo = repo;
        this.tokenService = tokenRepo;
        this.apiService = apiService;
    }

    public List<Event> getActualEvents() {
        List<Event> allEvents = repo.getAllEvents();

        long currentTimestamp = System.currentTimeMillis() / 1000;
        return allEvents.stream()
                .filter(event -> event.getToTimestamp() > currentTimestamp)
                .collect(Collectors.toList());
    }

    public void createEvent(Event event) {
        repo.create(event);
    }

    public Event readEvent(int id) {
        return repo.read(id);
    }

    public void updateEvent(Event event) {
        repo.update(event);
    }

    public void deleteEvent(int id) {
        repo.delete(id);
    }

    public Stats getEventStatsForCurrentUser(int eventId) {
        Event event = repo.read(eventId);
        int from = (int) event.getFromTimestamp();
        int to = (int) event.getToTimestamp();
        String eventType = event.getType();

        String token = getToken();

        List<Activity> eventActivities = apiService.athleteActivities(token, to, from);

        return new Stats(
                eventActivities.stream()
                        .filter(e -> e.getType().equalsIgnoreCase(eventType))
                        .collect(Collectors.toList())
        );
    }

    private String getToken() {
        Optional<MpToken> token = tokenService.get();

        if (token.isEmpty()) {
            throw new IllegalStateException("No token found");
        }

        return token.get().getAccessToken();
    }

    public Stats getActiveUserStats() {
        MpAthlete athlete = apiService.athlete(getToken());

        LocalDateTime createdDate = athlete.getCreatedAt();
        int userCreatedTimestamp = (int) Timestamp.valueOf(createdDate).getTime() / 1000;
        int currentTimestamp = (int) System.currentTimeMillis() / 1000;

        List<Activity> allActivities = apiService.athleteActivities(getToken(), currentTimestamp, userCreatedTimestamp);

        return new Stats(allActivities);
    }

    public List<Stats> getYearStats(int year) {
        long yearStartTimestamp = obtainYearTimestamp(year);
        long yearEndTimestamp = obtainYearTimestamp(year + 1);

        List<Activity> activities = apiService.athleteActivities(getToken(), yearEndTimestamp, yearStartTimestamp);

        return obtainPerMonthStats(activities, year);
    }

    private long obtainYearTimestamp(int year) {
        LocalDateTime yearStart = LocalDateTime.of(year, 1, 1, 0, 0);
        return Timestamp.valueOf(yearStart).getTime() / 1000;
    }

    private List<Stats> obtainPerMonthStats(List<Activity> activities, int year) {
        List<Stats> statsPerMonth = new ArrayList<>(12);

        for (int month = 1; month < 12; month++) {
            Stats monthStats = obtainMonthStats(activities, year, month);
            statsPerMonth.add(monthStats);
        }

        statsPerMonth.add(obtainMonthStats(activities, year, 12, year + 1, 1));

        return statsPerMonth;
    }

    private Stats obtainMonthStats(List<Activity> activities, int yearStart, int monthStart, int yearEnd, int monthEnd) {
        LocalDateTime monthDate = LocalDateTime.of(yearStart, monthStart, 1, 0, 0);
        LocalDateTime monthUpperBound = LocalDateTime.of(yearEnd, monthEnd, 1, 0, 0);
        List<Activity> monthActivities = activities.stream()
                .filter(e -> isBetween(monthDate, monthUpperBound, e))
                .toList();

        return new Stats(monthActivities);
    }

    private Stats obtainMonthStats(List<Activity> activities, int year, int month) {
        return obtainMonthStats(activities, year, month, year, month + 1);
    }

    private static boolean isBetween(LocalDateTime monthDate, LocalDateTime monthUpperBound, Activity e) {
        return e.getStartedDateTime().isAfter(monthDate) &&
                e.getStartedDateTime().isBefore(monthUpperBound);
    }

    public MpAthlete getAthlete(){
        MpAthlete athlete = apiService.athlete(getToken());

        int rating = RatingCalculator.calculateRating(repo.getAllEvents(), athlete);

        athlete.setRating(rating);

        return athlete;
    }
}
