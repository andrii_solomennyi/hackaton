package com.hackaton.controller;

import com.hackaton.dto.Event;
import com.hackaton.dto.MpAthlete;
import com.hackaton.dto.Stats;
import com.hackaton.service.EventService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EventController {
    private final EventService service;

    public EventController(EventService service) {
        this.service = service;
    }

    @GetMapping("api/actualEvents")
    public List<Event> getActualEvents() {
        return service.getActualEvents();
    }

    @PutMapping("api/event")
    public void createEvent(@RequestBody Event event) {
        service.createEvent(event);
    }

    @GetMapping("api/event/{id}")
    public Event readEvent(@PathVariable int id) {
        return service.readEvent(id);
    }

    @PostMapping("api/event")
    public void updateEvent(@RequestBody Event event) {
        service.updateEvent(event);
    }

    @DeleteMapping("api/event/{id}")
    public void deleteEvent(@PathVariable int id) {
        service.deleteEvent(id);
    }

    /**
     * Returns current user's stats for specified event
     *
     * @param eventId ID of the event to obtain stats for
     * @return event stats in JSON format
     */
    @GetMapping("api/event/{eventId}/stats")
    public Stats getEventStatsForCurrentUser(@PathVariable int eventId) {
        return service.getEventStatsForCurrentUser(eventId);
    }

    @GetMapping("api/user/stats")
    public Stats getActiveUserStats() {
        return service.getActiveUserStats();
    }

    @GetMapping("api/user/yearStats")
    public List<Stats> getPerMonthStatsForCurrentYear(@RequestParam int year){
        return service.getYearStats(year);
    }

    @GetMapping("api/user")
    public MpAthlete getAthlete(){
        return service.getAthlete();
    }
}
