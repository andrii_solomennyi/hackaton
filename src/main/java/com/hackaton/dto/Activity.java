package com.hackaton.dto;

import java.time.LocalDateTime;

public class Activity {
    private String type;
    private double distance;
    private double movingTime;
    private LocalDateTime startedDateTime;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getMovingTime() {
        return movingTime;
    }

    public void setMovingTime(double movingTime) {
        this.movingTime = movingTime;
    }

    public LocalDateTime getStartedDateTime() {
        return startedDateTime;
    }

    public void setStartedDateTime(LocalDateTime startedDateTime) {
        this.startedDateTime = startedDateTime;
    }
}
