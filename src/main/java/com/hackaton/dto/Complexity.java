package com.hackaton.dto;

public enum Complexity {
    LOWEST(10),
    LOW(20),
    NORMAL(30),
    HARD(50),
    EXTREME(80);

    private final int xp;

    Complexity(int xp) {
        this.xp = xp;
    }

    public int getXp() {
        return xp;
    }
}
