package com.hackaton.dto;

import java.util.HashSet;
import java.util.Set;

public class Event {

    private int id;
    private String name;
    private String description;
    private MpAthlete author;
    private String type;
    private long fromTimestamp;
    private long toTimestamp;
    private String location;
    private double distance;
    private Complexity complexity;
    private Set<MpAthlete> participants = new HashSet<>();

    public Event() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MpAthlete getAuthor() {
        return author;
    }

    public void setAuthor(MpAthlete author) {
        this.author = author;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getFromTimestamp() {
        return fromTimestamp;
    }

    public void setFromTimestamp(long fromTimestamp) {
        this.fromTimestamp = fromTimestamp;
    }

    public long getToTimestamp() {
        return toTimestamp;
    }

    public void setToTimestamp(long toTimestamp) {
        this.toTimestamp = toTimestamp;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Complexity getComplexity() {
        return complexity;
    }

    public void setComplexity(Complexity complexity) {
        this.complexity = complexity;
    }

    public Set<MpAthlete> getParticipants() {
        return new HashSet<>(participants);
    }

    public void setParticipants(Set<MpAthlete> participants) {
        this.participants = new HashSet<>(participants);
    }
}
