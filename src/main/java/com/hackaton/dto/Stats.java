package com.hackaton.dto;

public class Stats {
    private final double totalDistance;
    private final int elapsedTime;
    private final double averageKilometerSpeed;

    public Stats(Iterable<Activity> activities) {
        double totalDistance = 0;
        int elapsedTime = 0;

        for(Activity activity : activities) {
            totalDistance += activity.getDistance();
            elapsedTime += activity.getMovingTime();
        }

        this.totalDistance = totalDistance / 1000; // division by 1000 to obtain km from meters
        this.elapsedTime = elapsedTime / 60; // division to obtain minutes from seconds
        this.averageKilometerSpeed = elapsedTime == 0 ? 0 : totalDistance / elapsedTime;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public int getElapsedTime() {
        return elapsedTime;
    }

    public double getAverageKilometerSpeed() {
        return averageKilometerSpeed;
    }
}
