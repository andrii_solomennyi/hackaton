import { Button, Typography } from "@mui/material";
import React, { useState } from "react";

import { useAppSelector, useAppDispatch } from "../../app/hooks";
import { selectEvent } from "./eventSlice";

export function Counter() {
  const dispatch = useAppDispatch();
  const data = useAppSelector(selectEvent);

  return <></>;
}
