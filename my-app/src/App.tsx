import React from "react";
import Navigation from "./containers/header/Navigation";
import Home from "./containers/home/Home";

function App() {
  return (
    <div className="app">
      <Navigation />
    </div>
  );
}

export default App;
