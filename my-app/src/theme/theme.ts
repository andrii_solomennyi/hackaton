import { createTheme } from "@mui/material/styles";
import { Theme } from "@mui/material/styles";

declare module "@mui/system" {
  interface DefaultTheme extends Theme {}
}

const theme = createTheme({
  typography: {
    fontFamily: ["Poppins", "sans-serif"].join(","),
  },
  palette: {
    primary: {
      main: "#6077F7",
    },
    secondary: {
      main: "#4F4575",
    },
    text: {
      primary: "#fff",
      secondary: "#A7A2BA",
    },
  },
  components: {},
});

export default theme;
