import axios from "axios";

const baseUrl = "http://localhost:8080/api/";
const apikey = "actualEvents";

export const api = axios.create({
  baseURL: `${baseUrl}`,
  params: { apikey },
});
