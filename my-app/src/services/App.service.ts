import { api } from "../api/api";

class AppService {
  async getData() {
    return (await api.get("")).data;
  }
}
const appService = new AppService();

export default appService;
