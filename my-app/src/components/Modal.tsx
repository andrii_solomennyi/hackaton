import {
  Box,
  Button,
  FormControl,
  FormControlLabel,
  InputAdornment,
  Modal,
  Radio,
  RadioGroup,
  Stack,
  TextField,
  ThemeProvider,
  Typography,
} from "@mui/material";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { INewChallengeProps } from "./types";
import { createTheme } from "@mui/material/styles";
import { useState } from "react";
import { useForm, Controller } from "react-hook-form";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",

  width: 704,
  height: 796,
  paddingRight: 10,
  paddingTop: 10,
  paddingLeft: 10,
  color: "#000",
  bgcolor: "background.paper",
  border: "2px solid transparent",
  borderRadius: "12px",
  boxShadow: 24,
};

const themeForCalendar = createTheme({
  typography: {
    fontFamily: ["Poppins", "sans-serif"].join(","),
  },
  palette: {
    primary: {
      main: "#6077F7",
    },
    secondary: {
      main: "#4F4575",
    },
  },
});

const NewChallenge = ({ open, handleClose }: INewChallengeProps) => {
  const [value, setValue] = useState("");
  const [charCount, setCharCount] = useState(0);

  const handleChange = (event: any) => {
    setValue(event.target.value);
    setCharCount(event.target.value.length);
  };

  const defaultValues = {
    distance: "",
    duration: "",
    date: "",
    title: "",
  };
  const { handleSubmit, control, clearErrors } = useForm({
    mode: "onChange",
    defaultValues,
  });
  const handleCreate = () => {
    handleSubmit((data) => {
      console.log(data);
    })();
  };

  const notPast = (value: any) => {
    const selectedDate = new Date(value);
    const today = new Date();

    if (selectedDate < today) {
      return "Date cannot be in the past";
    }

    return true;
  };
  return (
    <>
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            sx={{ color: "secondary.main", fontWeight: 500, fontSize: "32px" }}
          >
            Create new{" "}
            <Box component="span" sx={{ color: "primary.main", fontStyle: "italic" }}>
              challenge
            </Box>
          </Typography>
          <Typography
            id="modal-modal-description"
            sx={{
              mt: 2,
              color: "secondary.main",
              fontWeight: 500,
              fontSize: 16,
            }}
          >
            Type of activity
          </Typography>

          <FormControl sx={{ mt: 2 }}>
            <RadioGroup row aria-labelledby="row-radio-buttons-group-label" name="row-radio-buttons-group">
              <Stack gap="10px" direction="row">
                <FormControlLabel value="bikeRide" control={<Radio />} label="Bike ride" />
                <FormControlLabel value="running" control={<Radio />} label="Running" />
                <FormControlLabel value="walking" control={<Radio />} label="Walking" />
              </Stack>
            </RadioGroup>

            <Typography sx={{ mt: "30px", fontWeight: 500, fontSize: "16px" }}>Title</Typography>

            <Controller
              name="title"
              control={control}
              rules={{
                required: "Title is required",
              }}
              render={({ field, fieldState: { invalid, error } }) => (
                <TextField
                  {...field}
                  error={invalid}
                  helperText={error?.message}
                  InputProps={{
                    sx: { color: "#000" },
                    endAdornment: <InputAdornment position="end" sx={{ mt: 2 }}>{`${charCount}/83`}</InputAdornment>,
                  }}
                  placeholder="Start typing..."
                  inputProps={{
                    maxLength: 83,
                  }}
                  id="title"
                  onKeyDown={handleChange}
                  variant="filled"
                  sx={{ width: 550 }}
                />
              )}
            />

            <Stack direction="row" gap="5px" sx={{ mt: "30px" }}>
              <Stack>
                <Typography sx={{ fontWeight: 500, fontSize: "16px" }}>Date</Typography>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <ThemeProvider theme={themeForCalendar}>
                    <Controller
                      name="date"
                      control={control}
                      rules={{
                        required: "Date is required",
                        validate: notPast,
                      }}
                      defaultValue=""
                      render={({ field, fieldState: { invalid, error } }) => (
                        <DesktopDatePicker
                          {...field}
                          sx={{
                            "& .MuiInputBase-input": {
                              padding: "25px 0 8px 12px",
                            },
                            "& .MuiButtonBase-root svg": {
                              marginTop: 2,
                            },
                          }}
                          disablePast
                        />
                      )}
                    />
                  </ThemeProvider>
                </LocalizationProvider>
              </Stack>
              <Stack>
                <Typography sx={{ fontWeight: 500, fontSize: "16px" }}>Duration</Typography>
                <Controller
                  name="duration"
                  control={control}
                  rules={{
                    required: "Duration is required",
                    min: {
                      value: 1,
                      message: "Must be at least 1",
                    },
                    max: {
                      value: 31,
                      message: "Must be not greater that 31",
                    },
                    pattern: {
                      value: /^[0-9]+$/,
                      message: "Must be a number",
                    },
                  }}
                  defaultValue=""
                  render={({ field, fieldState: { invalid, error } }) => (
                    <TextField
                      {...field}
                      error={invalid}
                      helperText={error?.message}
                      id="duration"
                      variant="filled"
                      placeholder="Start typing..."
                      InputProps={{
                        sx: {
                          color: "#000",
                        },
                        endAdornment: (
                          <InputAdornment position="end" sx={{ mt: 2 }}>
                            day
                          </InputAdornment>
                        ),
                      }}
                    />
                  )}
                />
              </Stack>
              <Stack>
                <Typography sx={{ fontWeight: 500, fontSize: "16px" }}>Distance</Typography>
                <Controller
                  control={control}
                  name="distance"
                  rules={{
                    required: "Distance is required",
                    min: {
                      value: 1,
                      message: "Must be at least 1",
                    },
                    pattern: {
                      value: /^[0-9]+$/,
                      message: "Must be a number",
                    },
                  }}
                  render={({ field, fieldState: { invalid, error } }) => (
                    <TextField
                      {...field}
                      error={invalid}
                      helperText={error?.message}
                      id="distance"
                      variant="filled"
                      placeholder="Start typing..."
                      InputProps={{
                        sx: { color: "#000" },
                        endAdornment: (
                          <InputAdornment position="end" sx={{ mt: 2 }}>
                            km
                          </InputAdornment>
                        ),
                      }}
                    />
                  )}
                />
              </Stack>
            </Stack>
            <Typography sx={{ mt: "30px", fontWeight: 500, fontSize: "16px" }}>Location</Typography>
            <TextField
              InputProps={{
                sx: { color: "#000" },
              }}
              id="title"
              placeholder="Start typing..."
              variant="filled"
              sx={{ width: 550 }}
            />
            <Typography sx={{ mt: "30px", fontWeight: 500, fontSize: "16px" }}>Add participants</Typography>
            <TextField
              InputProps={{
                sx: { color: "#000" },
              }}
              id="title"
              placeholder="Start typing user name"
              variant="filled"
              sx={{ width: 550 }}
            />
          </FormControl>
          <Stack sx={{ mt: 2 }}>
            <Box display="flex" justifyContent="flex-end">
              <Button
                variant="contained"
                onClick={handleCreate}
                sx={{ borderRadius: "16px", width: 132, textTransform: "none" }}
              >
                <Typography sx={{ fontSize: "16px", fontWeight: 500 }}>Create</Typography>
              </Button>
            </Box>
          </Stack>
        </Box>
      </Modal>
    </>
  );
};

export default NewChallenge;
