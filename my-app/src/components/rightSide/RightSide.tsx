import { styled } from "@mui/material";
import React, { PropsWithChildren } from "react";

const SideBarContainer = styled("div")(({ theme }) => ({
  background: theme.palette.secondary.main,
  height: "100vh",
  display: "flex",
  justifyContent: "flex-end",
  alignItems: "center",
  flexGrow: "1",
  // maxWidth: "00px",
}));

const RightSide = ({ children }: PropsWithChildren) => {
  return <SideBarContainer>{children}</SideBarContainer>;
};

export default RightSide;
