import React, { PropsWithChildren } from "react";
import SideBar from "../sideBar/SideBar";

const LeftSide = ({ children }: PropsWithChildren) => {
  return (
    <>
      <SideBar />
      <main className="main">{children}</main>
    </>
  );
};

export default LeftSide;
