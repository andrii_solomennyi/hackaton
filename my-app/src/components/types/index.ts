export interface INewChallengeProps {
  open: boolean;
  handleClose: () => void;
}
