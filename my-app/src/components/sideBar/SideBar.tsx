import React from "react";
import "./Header.css";
import StarIcon from "@mui/icons-material/Star";
import EventAvailableIcon from "@mui/icons-material/EventAvailable";
import { NavLink } from "react-router-dom";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import AssessmentOutlinedIcon from "@mui/icons-material/AssessmentOutlined";
const avatar = require("./avatar.png");

const SideBar = () => {
  return (
    <div className="header">
      <header className="header">
        <div className="bar-wrapper">
          <div className="logo"></div>
          <div className="profile-wrapper">
            <img src={avatar} className="profile-img" alt="" />
            <span className="name">Alexandra Leslie</span>
            <div className="rating">
              <span>eхperience:</span> <span>235</span>
            </div>
          </div>
          <div className="categories">
            <NavLink to="/">
              <div className="challenges categories-item" id="tab">
                <EventAvailableIcon />
                <span>Challenges</span>
              </div>
            </NavLink>
            <NavLink to="/profile">
              <div className="profile categories-item" id="tab">
                <PersonOutlineOutlinedIcon />

                <span>Profile</span>
              </div>
            </NavLink>
            <NavLink to="/settings">
              <div className="achievements categories-item" id="tab">
                <AssessmentOutlinedIcon />
                <span>Settings</span>
              </div>
            </NavLink>
          </div>
          <div className="logout">
            <span>Log out</span>
          </div>
        </div>
      </header>
    </div>
  );
};

export default SideBar;
