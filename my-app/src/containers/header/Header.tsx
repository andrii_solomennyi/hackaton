import { Avatar, AvatarGroup } from "@mui/material";
import React, { useState } from "react";
import "./Header.css";
import Navigation from "./Navigation";
import RunningSVG from "../../images/svg/Running";
import StarIcon from "@mui/icons-material/Star";
import EventAvailableIcon from "@mui/icons-material/EventAvailable";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import AssessmentOutlinedIcon from "@mui/icons-material/AssessmentOutlined";
import HikingIcon from "@mui/icons-material/Hiking";
import FilterAltOutlinedIcon from "@mui/icons-material/FilterAltOutlined";
import PedalBikeOutlinedIcon from "@mui/icons-material/PedalBikeOutlined";
import NearMeOutlinedIcon from "@mui/icons-material/NearMeOutlined";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import RotateLeftOutlinedIcon from "@mui/icons-material/RotateLeftOutlined";
import DirectionsRunRoundedIcon from "@mui/icons-material/DirectionsRunRounded";
import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import Settings from "../../images/svg/Settings";
import NewChallenge from "../../components/Modal";
const avatar = require("./avatar.png");
const eventTabId = "events-tab";
const tabId = "tab";

const Header = () => {
  const filter = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent> | React.MouseEvent<SVGSVGElement, MouseEvent | any>
  ) => {
    if (!event) {
      return;
    }
    const elements = document.querySelectorAll(`#${eventTabId}`);
    elements.forEach((element) => {
      element.classList.remove("activeTop");
    });
    event.currentTarget.classList.add("activeTop");
  };

  const activate = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (!event) {
      return;
    }
    const elements = document.querySelectorAll(`#${tabId}`);
    elements.forEach((element) => {
      element.classList.remove("active");
    });
    event.currentTarget.classList.add("active");
  };
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  return (
    <>
      <NewChallenge open={open} handleClose={handleClose} />
      <header className="header">
        <div className="bar-wrapper">
          <div className="logo"></div>
          <div className="profile-wrapper">
            <img src={avatar} className="profile-img" />
            <span className="name">Alexandra Leslie</span>
            <div className="rating">
              <span>eхperience:</span> <span>235</span>
            </div>
          </div>
          <div className="categories">
            <div className="challenges categories-item" id="tab" onClick={(e) => activate(e)}>
              <EventAvailableIcon />
              <span>Challenges</span>
            </div>
            <div className="profile categories-item" id="tab" onClick={(e) => activate(e)}>
              <PersonOutlineOutlinedIcon />
              <span>Profile</span>
            </div>
            <div className="achievements categories-item " id="tab" onClick={(e) => activate(e)}>
              <SettingsOutlinedIcon />
              <span>Settings</span>
            </div>
          </div>
          <div className="logout">
            <span>Log out</span>
          </div>
        </div>
      </header>
      <main className="main">
        <div className="create">
          <div className="create-inner-wrapper">
            <span className="title">Challenges </span>
            <div className="create-btn" onClick={handleOpen}>
              Create new
            </div>
          </div>
          <div className="events-categories">
            <div className="events-categories-main" id="events-categories-main">
              <div onClick={(e) => filter(e)} id="events-tab">
                <span>All</span>
              </div>

              <div onClick={(e) => filter(e)} id="events-tab">
                <span>Mine</span>
              </div>

              <div onClick={(e) => filter(e)} id="events-tab">
                <RunningSVG />
              </div>

              <HikingIcon onClick={(e) => filter(e)} id="events-tab" />
              <PedalBikeOutlinedIcon onClick={(e) => filter(e)} id="events-tab" />
            </div>
            <FilterAltOutlinedIcon />
          </div>
        </div>
        <div className="cards">
          <div className="card-wrapper">
            <div className="card-wrapper-inner">
              <div className="card" id="running">
                <div className="card-title">
                  <div className="circle">
                    <DirectionsRunRoundedIcon />
                  </div>

                  <span>Active running without obstacles</span>
                </div>
                <div className="infowrapper">
                  <div className="start">
                    <EventAvailableIcon />
                    Mar 29
                  </div>
                  <div className="duration">
                    <RotateLeftOutlinedIcon />
                    30 days
                  </div>
                  <div className="distance">
                    <NearMeOutlinedIcon />
                    18 km
                  </div>
                </div>
                <div className="location">
                  <div className="loca">
                    <LocationOnOutlinedIcon />
                  </div>
                  <div className="loc-description">
                    <span className="address">Martin Luther King・3210</span>
                    <span>Location pin</span>
                  </div>
                </div>
                <div className="people-take-part">
                  <div className="people-take-part-icons">
                    <AvatarGroup total={5} className="avatars">
                      <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
                      <Avatar alt="Travis Howard" src="/static/images/avatar/2.jpg" />
                      <Avatar alt="Agnes Walker" src="/static/images/avatar/4.jpg" />
                      <Avatar alt="Trevor Henderson" src="/static/images/avatar/5.jpg" />
                    </AvatarGroup>
                  </div>
                  <div className="apply-btn">APPLY</div>
                </div>
              </div>
            </div>
          </div>

          <div className="card-wrapper">
            <div className="card-wrapper-inner">
              <div className="card" id="hiking">
                <div className="card-title">
                  <div className="circle">
                    <HikingIcon />
                  </div>

                  <span>Active running without obstacles</span>
                </div>
                <div className="infowrapper">
                  <div className="start">
                    <EventAvailableIcon />
                    Mar 29
                  </div>
                  <div className="duration">
                    <RotateLeftOutlinedIcon />
                    30 days
                  </div>
                  <div className="distance">
                    <NearMeOutlinedIcon />
                    18 km
                  </div>
                </div>
                <div className="location">
                  <div className="loca">
                    <LocationOnOutlinedIcon />
                  </div>
                  <div className="loc-description">
                    <span className="address">Martin Luther King・3210</span>
                    <span>Location pin</span>
                  </div>
                </div>
                <div className="people-take-part">
                  <div className="people-take-part-icons">
                    <AvatarGroup total={5} className="avatars">
                      <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
                      <Avatar alt="Travis Howard" src="/static/images/avatar/2.jpg" />
                      <Avatar alt="Agnes Walker" src="/static/images/avatar/4.jpg" />
                      <Avatar alt="Trevor Henderson" src="/static/images/avatar/5.jpg" />
                    </AvatarGroup>
                  </div>
                  <div className="apply-btn">APPLY</div>
                </div>
              </div>
            </div>
          </div>

          <div className="card-wrapper">
            <div className="card-wrapper-inner">
              <div className="card" id="cycling">
                <div className="card-title">
                  <div className="circle">
                    <PedalBikeOutlinedIcon />
                  </div>

                  <span>Active running without obstacles</span>
                </div>
                <div className="infowrapper">
                  <div className="start">
                    <EventAvailableIcon />
                    Mar 29
                  </div>
                  <div className="duration">
                    <RotateLeftOutlinedIcon />
                    30 days
                  </div>
                  <div className="distance">
                    <NearMeOutlinedIcon />
                    18 km
                  </div>
                </div>
                <div className="location">
                  <div className="loca">
                    <LocationOnOutlinedIcon />
                  </div>
                  <div className="loc-description">
                    <span className="address">Martin Luther King・3210</span>
                    <span>Location pin</span>
                  </div>
                </div>
                <div className="people-take-part">
                  <div className="people-take-part-icons">
                    <AvatarGroup total={5} className="avatars">
                      <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
                      <Avatar alt="Travis Howard" src="/static/images/avatar/2.jpg" />
                      <Avatar alt="Agnes Walker" src="/static/images/avatar/4.jpg" />
                      <Avatar alt="Trevor Henderson" src="/static/images/avatar/5.jpg" />
                    </AvatarGroup>
                  </div>
                  <div className="apply-btn">APPLY</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default Header;
