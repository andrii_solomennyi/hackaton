import Home from "../home/Home";
import Profile from "../profile/Profile";

import { IRoute } from "./types/index";

export const routes: IRoute[] = [
  {
    path: "/",
    component: <Home />,
  },
  {
    path: "/profile",
    component: <Profile />,
  },
];
