import React, { useEffect, useState } from "react";
import LeftSide from "../../components/leftSide/LeftSide";
import Main from "../main/Main";
import "./Profile.css";

const Profile = () => {
  const [distance, setDistance] = useState<never[] | any>([]);
  useEffect(() => {
    if (distance || window.location.href === "http://localhost:3000/profile") {
      renderChart();
      return;
    } else {
      fetch("http://localhost:8080/api/user/yearStats?year=2017").then((item) => setDistance(item.json()));
    }
  }, [distance]);

  const renderChart = () => {
    const items = document.querySelectorAll(".chart-item") as NodeListOf<HTMLDivElement>;
    for (let i = 0; i < items.length; i++) {
      items[i].style.height = distance[i].totalDistance * 20 + "px";
    }
  };
  return (
    <>
      <LeftSide>
        <div className="stats">
          <div className="stats-title">
            <span>My expirience:</span>
            <span>235</span>
          </div>
          <div className="chart-wrapper">
            <div className="size-bar">
              <span>10</span>
              <span>20</span>
              <span>30</span>
              <span>40</span>
              <span>50</span>
              <span>60</span>
              <span>70</span>
              <span>80</span>
              <span>90</span>
              <span>100</span>
              <span>Total Distance</span>
            </div>

            <div className="chart">
              <div className="january chart-item">
                <span>jan</span>
              </div>
              <div className="february chart-item">
                <span>feb</span>
              </div>
              <div className="march chart-item">
                <span>mar</span>
              </div>
              <div className="april chart-item">
                <span>apr</span>
              </div>
              <div className="may chart-item">
                <span>may</span>
              </div>
              <div className="june chart-item">
                <span>jun</span>
              </div>
              <div className="july chart-item">
                <span>jul</span>
              </div>
              <div className="august chart-item">
                <span>aug</span>
              </div>
              <div className="september chart-item">
                <span>sep</span>
              </div>
              <div className="october chart-item">
                <span>oct</span>
              </div>
              <div className="november chart-item">
                <span>nov</span>
              </div>
              <div className="december chart-item">
                <span>dec</span>
              </div>
            </div>
          </div>
        </div>
      </LeftSide>
      <Main />;
    </>
  );
};

export default Profile;
