import React from "react";
import { IAchieves } from ".";
import { achievements } from "./achievements";

const AchivementsBar = () => {
  const renderAchieves = ({ text, url }: IAchieves) => (
    <div className="achieve">
      <img src={url} alt="" />
      <span>text</span>
    </div>
  );
  return <div className="achivements-bar">{achievements.map(renderAchieves)}</div>;
};

export default AchivementsBar;
