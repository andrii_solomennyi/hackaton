import { IAchieves } from "./index";

export const achievements: IAchieves[] = [
  {
    text: "First activity",
    url: "../../../images/png/FirstActivity.png",
  },
  {
    text: "Good friend",
    url: "../../../images/png/GoodFriend.png",
  },
  {
    text: "Wow. Good job",
    url: "../../../images/png/WowGoodjob.png",
  },
  {
    text: "Jobs",
    url: "../../../images/png/WowGoodjob.png",
  },
  {
    text: "Help",
    url: "../../../images/png/WowGoodjob.png",
  },
  {
    text: "API",
    url: "../../../images/png/WowGoodjob.png",
  },
  {
    text: "Privacy",
    url: "../../../images/png/WowGoodjob.png",
  },
  {
    text: "Terms",
    url: "../../../images/png/WowGoodjob.png",
  },
  {
    text: "Top accounts",
    url: "../../../images/png/WowGoodjob.png",
  },
  {
    text: "Locations",
    url: "../../../images/png/WowGoodjob.png",
  },
  {
    text: "Instagram Lite",
    url: "../../../images/png/WowGoodjob.png",
  },
  {
    text: "Contact uploading and non-users",
    url: "../../../images/png/WowGoodjob.png",
  },
];
