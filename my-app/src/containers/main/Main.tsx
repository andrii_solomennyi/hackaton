import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import { Box, Divider, Typography } from "@mui/material";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { Stack, styled } from "@mui/system";
import { StaticDatePicker } from "@mui/x-date-pickers";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import HikingIcon from "@mui/icons-material/Hiking";
import DirectionsRunIcon from "@mui/icons-material/DirectionsRun";

const SideBarContainer = styled("div")({
  background: "#4F4575",
  height: "100vh",
  display: "flex",
  justifyContent: "flex-end",
  alignItems: "center",
  width: "400px",
  margin: " 0 0 0 20px",
});

const StyledDivider = styled(Divider)({
  width: 240,
  height: 1,
  background: "#E5E5E5",
  marginTop: "8px",
  left: 256,
});

const Main = () => {
  return (
    <>
      <SideBarContainer>
        <Stack direction={"column"} marginRight={"40px"}>
          <Stack direction={"row"} justifyContent={"flex-end"} spacing={0.5} alignItems={"center"} sx={{ mb: "36px" }}>
            <HelpOutlineIcon
              sx={{
                color: "text.secondary",
                width: "13.33px",
                height: "13.33px",
              }}
            />
            <Typography sx={{ color: "text.secondary", fontSize: 14 }}>Help</Typography>
          </Stack>
          <Typography sx={{ fontSize: "20px", fontWeight: 500, lineHeight: "24px" }}>Applayed challenges </Typography>
          <StyledDivider />
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <StaticDatePicker
              autoFocus={true}
              sx={{
                mt: "8px",
                backgroundColor: "secondary.main",
                "& button": {
                  backgroundColor: "transparent",
                },
                "& svg": {
                  color: "#F2F2F7",
                },
                "& .MuiPickersToolbar-root h4": {
                  fontSize: "16px",
                },
                "& .MuiPickersToolbar-root": {
                  padding: 0,
                },
                "& .MuiPickersCalendarHeader-root": {
                  fontSize: "16px",
                  padding: 0,
                },
                "& .MuiDialogActions-root": {
                  display: "none",
                },
                "& .MuiPickersFadeTransitionGroup-root": {
                  marginRight: 5,
                },
                "& .MuiPickersArrowSwitcher-root": {
                  marginRight: 5,
                },
              }}
            />
          </LocalizationProvider>
          <Stack>
            <Stack direction={"row"} alignItems={"center"} marginBottom={"8px"}>
              <Box
                sx={{
                  background: "#FFEEE8",
                  width: 40,
                  height: 40,
                  borderRadius: "50%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  mr: 2,
                }}
              >
                <HikingIcon sx={{ color: "#FF7B43" }} />
              </Box>
              <Typography
                sx={{
                  fontSize: 16,
                  fontWeight: 500,
                  color: "#F7F7F7",
                  width: 184,
                }}
              >
                Go hiking with us
              </Typography>
            </Stack>
            <Stack direction={"row"} alignItems={"center"}>
              <Typography
                sx={{
                  mr: 15,
                  color: "text.secondary",
                  fontSize: "14px",
                  lineHeight: "18.2px",
                }}
              >
                14.03.2023
              </Typography>
              <Typography sx={{ fontSize: "12px" }}>More</Typography>
              <KeyboardArrowRightIcon sx={{ width: 20, height: 20 }} />
            </Stack>
            <StyledDivider sx={{ borderRadius: "12px" }} />
          </Stack>

          <Stack sx={{ mt: "40px" }}>
            <Stack direction={"row"} alignItems={"center"} marginBottom={"8px"}>
              <Box
                sx={{
                  background: "#EDF2FF",
                  width: 40,
                  height: 40,
                  borderRadius: "50%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  mr: 2,
                }}
              >
                <DirectionsRunIcon sx={{ color: "#517CFD" }} />
              </Box>
              <Typography
                sx={{
                  fontSize: 16,
                  fontWeight: 500,
                  color: "#F7F7F7",
                  width: 184,
                }}
              >
                Active running without obstacles
              </Typography>
            </Stack>
            <Stack direction={"row"} alignItems={"center"}>
              <Typography
                sx={{
                  mr: 15,
                  color: "text.secondary",
                  fontSize: "14px",
                  lineHeight: "18.2px",
                }}
              >
                14.03.2023
              </Typography>
              <Typography sx={{ fontSize: "12px" }}>More</Typography>
              <KeyboardArrowRightIcon sx={{ width: 20, height: 20 }} />
            </Stack>
            <StyledDivider sx={{ borderRadius: "12px" }} />
          </Stack>
        </Stack>
      </SideBarContainer>
    </>
  );
};

export default Main;
