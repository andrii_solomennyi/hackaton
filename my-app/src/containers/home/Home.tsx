import React, { useEffect, useState } from "react";
import { Avatar, AvatarGroup } from "@mui/material";
import LeftSide from "../../components/leftSide/LeftSide";
import SideBar from "../../components/sideBar/SideBar";
import RunningSVG from "../../images/svg/Running";
import Header from "../header/Header";
import Main from "../main/Main";
import StarIcon from "@mui/icons-material/Star";
import EventAvailableIcon from "@mui/icons-material/EventAvailable";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import AssessmentOutlinedIcon from "@mui/icons-material/AssessmentOutlined";
import HikingIcon from "@mui/icons-material/Hiking";
import FilterAltOutlinedIcon from "@mui/icons-material/FilterAltOutlined";
import PedalBikeOutlinedIcon from "@mui/icons-material/PedalBikeOutlined";
import NearMeOutlinedIcon from "@mui/icons-material/NearMeOutlined";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import RotateLeftOutlinedIcon from "@mui/icons-material/RotateLeftOutlined";
import DirectionsRunRoundedIcon from "@mui/icons-material/DirectionsRunRounded";
import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import Settings from "../../images/svg/Settings";
import NewChallenge from "../../components/Modal";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { fetchData, selectEvent } from "../../features/counter/eventSlice";
import appService from "../../services/App.service";

const eventTabId = "events-tab";
const tabId = "tab";

const Home = () => {
  const filter = (
    event:
      | React.MouseEvent<HTMLSpanElement, MouseEvent>
      | React.MouseEvent<SVGSVGElement, MouseEvent | any>
  ) => {
    if (!event) {
      return;
    }
    const elements = document.querySelectorAll(`#${eventTabId}`);
    elements.forEach((element) => {
      element.classList.remove("activeTop");
    });
    event.currentTarget.classList.add("activeTop");
  };

  const activate = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (!event) {
      return;
    }
    const elements = document.querySelectorAll(`#${tabId}`);
    elements.forEach((element) => {
      element.classList.remove("active");
    });
    event.currentTarget.classList.add("active");
  };
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [actualEvents, setActualEvents] = useState([]);
  useEffect(() => {
    const fetchActualEvents = async () => {
      const response = await fetch("http://localhost:8080/api/user");
      const data = await response.json();
      setActualEvents(data);
    };
    fetchActualEvents();
  }, []);

  console.log(actualEvents);

  return (
    <>
      <NewChallenge open={open} handleClose={handleClose} />
      <NewChallenge open={open} handleClose={handleClose} />
      <LeftSide>
        <div className="create">
          <div className="create-inner-wrapper">
            <span className="title">Challenges </span>
            <div className="create-btn" onClick={handleOpen}>
              Create new
            </div>
          </div>
          <div className="events-categories">
            <div className="events-categories-main" id="events-categories-main">
              <div onClick={(e) => filter(e)} id="events-tab">
                <span>All</span>
              </div>

              <div onClick={(e) => filter(e)} id="events-tab">
                <span>Mine</span>
              </div>

              <div onClick={(e) => filter(e)} id="events-tab">
                <RunningSVG />
              </div>

              <HikingIcon onClick={(e) => filter(e)} id="events-tab" />
              <PedalBikeOutlinedIcon
                onClick={(e) => filter(e)}
                id="events-tab"
              />
            </div>
            <FilterAltOutlinedIcon />
          </div>
        </div>
        <div className="cards">
          <div className="card-wrapper">
            <div className="card-wrapper-inner">
              <div className="card" id="running">
                <div className="card-title">
                  <div className="circle">
                    <DirectionsRunRoundedIcon />
                  </div>

                  <span>Active running without obstacles</span>
                </div>
                <div className="infowrapper">
                  <div className="start">
                    <EventAvailableIcon />
                    Mar 29
                  </div>
                  <div className="duration">
                    <RotateLeftOutlinedIcon />
                    30 days
                  </div>
                  <div className="distance">
                    <NearMeOutlinedIcon />
                    18 km
                  </div>
                </div>
                <div className="location">
                  <div className="loca">
                    <LocationOnOutlinedIcon />
                  </div>
                  <div className="loc-description">
                    <span className="address">Martin Luther King・3210</span>
                    <span>Location pin</span>
                  </div>
                </div>
                <div className="people-take-part">
                  <div className="people-take-part-icons">
                    <AvatarGroup total={5} className="avatars">
                      <Avatar
                        alt="Remy Sharp"
                        src="/static/images/avatar/1.jpg"
                      />
                      <Avatar
                        alt="Travis Howard"
                        src="/static/images/avatar/2.jpg"
                      />
                      <Avatar
                        alt="Agnes Walker"
                        src="/static/images/avatar/4.jpg"
                      />
                      <Avatar
                        alt="Trevor Henderson"
                        src="/static/images/avatar/5.jpg"
                      />
                    </AvatarGroup>
                  </div>
                  <div className="apply-btn">APPLY</div>
                </div>
              </div>
            </div>
          </div>

          <div className="card-wrapper">
            <div className="card-wrapper-inner">
              <div className="card" id="hiking">
                <div className="card-title">
                  <div className="circle">
                    <HikingIcon />
                  </div>

                  <span>Active running without obstacles</span>
                </div>
                <div className="infowrapper">
                  <div className="start">
                    <EventAvailableIcon />
                    Mar 29
                  </div>
                  <div className="duration">
                    <RotateLeftOutlinedIcon />
                    30 days
                  </div>
                  <div className="distance">
                    <NearMeOutlinedIcon />
                    18 km
                  </div>
                </div>
                <div className="location">
                  <div className="loca">
                    <LocationOnOutlinedIcon />
                  </div>
                  <div className="loc-description">
                    <span className="address">Martin Luther King・3210</span>
                    <span>Location pin</span>
                  </div>
                </div>
                <div className="people-take-part">
                  <div className="people-take-part-icons">
                    <AvatarGroup total={5} className="avatars">
                      <Avatar
                        alt="Remy Sharp"
                        src="/static/images/avatar/1.jpg"
                      />
                      <Avatar
                        alt="Travis Howard"
                        src="/static/images/avatar/2.jpg"
                      />
                      <Avatar
                        alt="Agnes Walker"
                        src="/static/images/avatar/4.jpg"
                      />
                      <Avatar
                        alt="Trevor Henderson"
                        src="/static/images/avatar/5.jpg"
                      />
                    </AvatarGroup>
                  </div>
                  <div className="apply-btn">APPLY</div>
                </div>
              </div>
            </div>
          </div>

          <div className="card-wrapper">
            <div className="card-wrapper-inner">
              <div className="card" id="cycling">
                <div className="card-title">
                  <div className="circle">
                    <PedalBikeOutlinedIcon />
                  </div>

                  <span>Active running without obstacles</span>
                </div>
                <div className="infowrapper">
                  <div className="start">
                    <EventAvailableIcon />
                    Mar 29
                  </div>
                  <div className="duration">
                    <RotateLeftOutlinedIcon />
                    30 days
                  </div>
                  <div className="distance">
                    <NearMeOutlinedIcon />
                    18 km
                  </div>
                </div>
                <div className="location">
                  <div className="loca">
                    <LocationOnOutlinedIcon />
                  </div>
                  <div className="loc-description">
                    <span className="address">Martin Luther King・3210</span>
                    <span>Location pin</span>
                  </div>
                </div>
                <div className="people-take-part">
                  <div className="people-take-part-icons">
                    <AvatarGroup total={5} className="avatars">
                      <Avatar
                        alt="Remy Sharp"
                        src="/static/images/avatar/1.jpg"
                      />
                      <Avatar
                        alt="Travis Howard"
                        src="/static/images/avatar/2.jpg"
                      />
                      <Avatar
                        alt="Agnes Walker"
                        src="/static/images/avatar/4.jpg"
                      />
                      <Avatar
                        alt="Trevor Henderson"
                        src="/static/images/avatar/5.jpg"
                      />
                    </AvatarGroup>
                  </div>
                  <div className="apply-btn">APPLY</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </LeftSide>
      <Main />
    </>
  );
};

export default Home;
